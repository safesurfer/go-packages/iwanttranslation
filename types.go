package iwanttranslation

import (
	"context"

	gcpApi "cloud.google.com/go/translate/apiv3"
)

type GCPTranslatorConfig struct {
	// The gcp service account JSON file that will allow
	// access to the translation API when used as auth.
	Credentials string `yaml:"credentials"`
	// The GCP project ID to use.
	Project string `yaml:"project"`
}

type TranslatorConfig struct {
	// If not nil, use the Google Cloud Translate API.
	GCP *GCPTranslatorConfig `yaml:"gcp"`
	// The max length of text to allow. If text is submitted longer than
	// this, it will be silently truncated to this length.
	MaxLen int `yaml:"maxLen"`
}

type gcpDriver struct {
	config *GCPTranslatorConfig
	client *gcpApi.TranslationClient
}

type translatorInstance struct {
	config *TranslatorConfig
	driver TranslatorDriver
}

type TranslatorDriver interface {
	Translate(ctx context.Context, sourceLanguage, targetLanguage, text string) (translated string, err error)
	Detect(ctx context.Context, text string) (language string, err error)
	Close() error
}

type Translator interface {
	GetConfig() *TranslatorConfig
	TranslatorDriver
}
