package iwanttranslation

import (
	"context"
	"errors"
	"fmt"

	gcpApi "cloud.google.com/go/translate/apiv3"
	"google.golang.org/api/option"
	"google.golang.org/genproto/googleapis/cloud/translate/v3"
)

func newGcpDriver(ctx context.Context, config *GCPTranslatorConfig) (driver *gcpDriver, err error) {
	driver = &gcpDriver{
		config: config,
	}
	driver.client, err = gcpApi.NewTranslationClient(ctx, option.WithCredentialsJSON([]byte(config.Credentials)))
	if err != nil {
		return nil, err
	}
	return driver, nil
}

func (g *gcpDriver) Translate(ctx context.Context, sourceLanguage, targetLanguage, text string) (translated string, err error) {
	resp, err := g.client.TranslateText(ctx, &translate.TranslateTextRequest{
		Parent:             fmt.Sprintf("projects/%v", g.config.Project),
		Contents:           []string{text},
		MimeType:           "text/plain",
		SourceLanguageCode: sourceLanguage,
		TargetLanguageCode: targetLanguage,
	})
	if err != nil {
		return "", err
	}
	if len(resp.Translations) == 0 {
		return "", errors.New("GCP translation API returned no results")
	}
	return resp.Translations[0].TranslatedText, nil
}

func (g *gcpDriver) Detect(ctx context.Context, text string) (language string, err error) {
	resp, err := g.client.DetectLanguage(ctx, &translate.DetectLanguageRequest{
		Parent:   fmt.Sprintf("projects/%v", g.config.Project),
		Source:   &translate.DetectLanguageRequest_Content{Content: text},
		MimeType: "text/plain",
	})
	if err != nil {
		return "", err
	}
	if len(resp.Languages) == 0 {
		return "", nil
	}
	return resp.Languages[0].LanguageCode, nil
}

func (g *gcpDriver) Close() error {
	return g.client.Close()
}
