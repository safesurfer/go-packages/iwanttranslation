package iwanttranslation

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"

	"golang.org/x/text/language"
	yaml "gopkg.in/yaml.v3"
)

type ErrBadConfig struct {
	Err error
}

func (b *ErrBadConfig) Error() string {
	return fmt.Sprintf("iwanttranslation: bad config: %v", b.Err.Error())
}

func New(ctx context.Context, config *TranslatorConfig) (translator Translator, err error) {
	translatorInstance := &translatorInstance{}
	translatorInstance.config = config
	if config.GCP != nil {
		translatorInstance.driver, err = newGcpDriver(ctx, config.GCP)
		if err != nil {
			return nil, err
		}
	} else {
		return nil, &ErrBadConfig{errors.New("config.GCP must be set")}
	}
	return translatorInstance, nil
}

func NewFromEnv(ctx context.Context) (Translator, error) {
	maxLenStr := os.Getenv("I_WANT_TRANSLATION_MAX_LEN")
	maxLen, err := strconv.Atoi(maxLenStr)
	if err != nil {
		return nil, &ErrBadConfig{fmt.Errorf("bad value for env I_WANT_TRANSLATION_MAX_LEN %v: %v", maxLenStr, err)}
	}
	config := &TranslatorConfig{
		MaxLen: maxLen,
	}
	driver := os.Getenv("I_WANT_TRANSLATION_DRIVER")
	switch driver {
	case "GCP":
		credsPath := os.Getenv("I_WANT_TRANSLATION_GCP_CREDENTIALS_PATH")
		credentials, err := ioutil.ReadFile(credsPath)
		if err != nil {
			return nil, &ErrBadConfig{fmt.Errorf("bad value for env I_WANT_TRANSLATION_GCP_CREDENTIALS_PATH %v: %v", credsPath, err)}
		}
		config.GCP = &GCPTranslatorConfig{
			Credentials: string(credentials),
			Project:     os.Getenv("I_WANT_TRANSLATION_GCP_PROJECT"),
		}
	default:
		return nil, &ErrBadConfig{fmt.Errorf("bad value for env I_WANT_TRANSLATION_DRIVER %v: valid choices are GCP", driver)}
	}
	return New(ctx, config)
}

func NewFromConfigFile(ctx context.Context, path string) (Translator, error) {
	configFile, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, &ErrBadConfig{fmt.Errorf("file %v couldn't be read: %v", path, err)}
	}
	var config TranslatorConfig
	err = yaml.Unmarshal(configFile, &config)
	if err != nil {
		return nil, &ErrBadConfig{fmt.Errorf("file %v couldn't be converted from yaml: %v", path, err)}
	}
	return New(ctx, &config)
}

// Limit a string to n characters.
func trimText(in string, n int) string {
	endSlice := n
	if len(in) < n {
		endSlice = len(in)
	}
	return in[0:endSlice]
}

type ErrBadLanguageCode struct {
	Err error
}

func (e *ErrBadLanguageCode) Error() string {
	return fmt.Sprintf("iwanttranslation: bad language code: %v", e.Err.Error())
}

func (t *translatorInstance) Translate(ctx context.Context, sourceLanguage, targetLanguage, text string) (translated string, err error) {
	_, err = language.Parse(sourceLanguage)
	if err != nil {
		return "", &ErrBadLanguageCode{fmt.Errorf("bad sourceLanguage %v: %v", sourceLanguage, err)}
	}
	_, err = language.Parse(targetLanguage)
	if err != nil {
		return "", &ErrBadLanguageCode{fmt.Errorf("bad targetLanguage %v: %v", targetLanguage, err)}
	}
	text = trimText(text, t.config.MaxLen)
	return t.driver.Translate(ctx, sourceLanguage, targetLanguage, text)
}

func (t *translatorInstance) Detect(ctx context.Context, text string) (language string, err error) {
	return t.driver.Detect(ctx, trimText(text, t.config.MaxLen))
}

func (t *translatorInstance) Close() error {
	return t.driver.Close()
}

func (t *translatorInstance) GetConfig() *TranslatorConfig {
	return t.config
}
